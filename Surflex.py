from bzrlib.urlutils import join
import glob
import re
import os
import random
__author__ = 'kajtek'


from os import mkdir

serviceflag = True

class BestScoreGenerator:
    """
    Best score generator class.
    Returns best scored records from default Surflex output file.
    File structure:
    *_000 [score] [score] [score]
    *_001 [score] [score] [score]
    *_002 [score] [score] [score]
    ...
    """
########################################################################################################################
    def __init__(self, f):
        """
        @param f:str - path to data file
        """
        self.f = open(f, 'r')
        # print(self.f)
########################################################################################################################

    def genbest(self):
        """
        best score generator
        """
        for __line in self.f.readlines():
            __match = re.search(r'.*\d+_000', __line)
            if __match is not None:
                yield __line.strip('\n')

########################################################################################################################
########################################################################################################################

class CatalogParser:
    """
    Class providing methods used to access input files
    """
########################################################################################################################

    def __init__(self, path):
        """
        @param path:str - data catalog path
        """
        self.path = path
########################################################################################################################

    def listAllLogFiles(self):
        """
        Returns list of all files with .log extension
        """
        self.list = glob.glob(self.path + "/*.log")

        self.list.sort()
        filelist = []
        for item in self.list:
            filelist.append(item)
        return filelist
########################################################################################################################

    def listsingleProt(self, name):
        """
        Returns a list of files describing single protein according to the following model:
        *[custom name]*.log
        """
        filelist = glob.glob((self.path + "/*" + name + "*.log"))
        filelist.sort()
        return filelist


########################################################################################################################

    def listSingleProtTemporaryFiles(self, name="STAT"):

        """
        Returns list of all files named [name]*.temp
        @param name:str - protein name, default - STAT
        """
        return sorted(glob.glob(name +'*.temp'))
########################################################################################################################

    def getSmallestTemporaryFile(self, temporaryDirectory="Temporary_Directory"):
        """
        Returns path to the shortest file in Temporary Directory
        @param temporaryDirectory: absolute path to Temporary Directory catalog
            Default - "Temporary_Directory" in data folder
        """
        shortest = str
        length = -1
        if temporaryDirectory == "Temporary_Directory":
            temporaryDirectory = self.path + "Temporary_Directory"
        for filePath in glob.glob(temporaryDirectory):
            if self.filelen(filePath) < length or length == -1:
                shortest = filePath
                length = self.filelen(file)
        return shortest

########################################################################################################################

    def filelen(self, FileName):
        """
        Returns number of lines in FileName file
        """
        with open(FileName) as f:
            for i, l in enumerate(f):
                pass
        return i + 1



class FileJoiner:
    """
    Allows to join data from multiple input files
    """
########################################################################################################################
    def __init__(self, path, diffprot=[],temporarydir="Temporary_Directory",differentialfile=""):
        self.path = path
        self.temporaryDir = temporarydir
        self.diffprot = diffprot
        self.cp = CatalogParser(self.path)
        self.createTemporarydir()
        self.differentialfile = differentialfile


########################################################################################################################
    def createTemporarydir(self):
        """
        Creates temporary directory and sets working directory to it
        """
        if not os.path.exists(self.path + '/' + self.temporaryDir):
            mkdir(self.path + '/' + self.temporaryDir)
        os.chdir(self.path + '/' + self.temporaryDir )
    ########################################################################################################################
    def generatefilename(self, file, diffprot):
        with open(file) as f:
            f.readline()
            tmp = f.readline()
            regex = re.search(".*(p.*)-result.*", tmp)
            if regex is not  None:
                return regex.group(1) +"_"+ str(diffprot) + "_differential_score"
            else:
                return str(diffprot) + "_differential_score"
    def joinWithinProts(self):
        """
        Joins multiple files regarding single protein
        """
        uniquestats = set()
        for file in self.cp.listAllLogFiles():
            __match = re.search(r'.*_(.*)_\d.*', file)
            if __match is not None:
                uniquestats.add(__match.group(1))

        for uniqueitem in uniquestats:
            outfile = open(uniqueitem + "_temporary.temp", 'w')
            outfile.write("#########USED FILES:\n")
            for file in self.cp.listsingleProt(uniqueitem):
                outfile.write('# ' + file + '\n')
            outfile.write('####################\n\n')
            for file in self.cp.listsingleProt(uniqueitem):
                bs = BestScoreGenerator(file)
                print("bs " + file)
                for line in bs.genbest():
                    outfile.write(line + '\n')
    def joinAllProts(self):
        """
        Creates file joining data from all protein files
        """

        cp = CatalogParser(self.temporaryDir)
        pathlist = list()
        pathlist.extend(cp.listSingleProtTemporaryFiles())

        referencefile = open(pathlist[0])
        del pathlist[0]
        filelist = [open(file, 'r') for file in pathlist]

        outfile = open(self.path + "/" +self.temporaryDir + "/joined_table.temp", 'w+')
        outfile.write(" " +(re.search(r'(STAT\d+[abAB]?)', referencefile.name)).group(0) + "   ")
        for tmpfilename in filelist:
            outfile.write((re.search(r'(STAT\d+[abAB]?)', tmpfilename.name)).group(0) + "   ")
        outfile.write('\n')
        referencelines = referencefile.readlines()
        cmplines = []

        for file in filelist:
            cmplines.append(file.readlines())

        for i in range(0,cmplines.__len__()):
            cmplines[i].sort()

        for i in range(0,referencelines.__len__()):
            fixedline = []
            linematch = re.match(r'(.*\d+_000)', referencelines[i])
            if linematch is None:
                continue
            fixedline.append(referencelines[i].strip('\n'))
            [file.seek(0) for file in filelist]

            for i2 in range(0,cmplines.__len__()):
                for i3 in range(0, len(cmplines[i2])):
                    regexTemp = linematch.group(0) + r' (.*)'
                    innerLineMatch = re.match(regexTemp, cmplines[i2][i3])

                    if innerLineMatch is not None:
                        fixedline.append(' ' +innerLineMatch.group(1))
                        del cmplines[i2][i3]
                        break


            if fixedline.__len__() == filelist.__len__()+1:
                outfile.write(''.join(fixedline) + '\n')
        return  outfile

    def diff(self, difftarget, joinedtablefile):
        """
        Creates tsv file containing differential scores
        """
        joinedtablefile.seek(0)
        cp = CatalogParser(self.temporaryDir)
        pathlist = list()
        pathlist.extend(cp.listSingleProtTemporaryFiles())
        print(pathlist)
        print(self.path+ "/"+ self.generatefilename(pathlist[pathlist.__len__()-1], difftarget) + ".tsv")
        outfile = open(self.path+ "/" + self.generatefilename(pathlist[pathlist.__len__()-1], difftarget) + ".tsv", 'w+')
        tmp = pathlist.pop()
        pathlist.insert(0,tmp)
        outfile.write("ID\t" + difftarget + "\t")
        pathlist = sorted(pathlist)
        print(pathlist)
        for file in pathlist:
            reg = re.search(r'(STAT\d+[abAB]?)', file)
            if reg is not None and not reg.group(0).__contains__(difftarget):
                outfile.write(difftarget + "-" + reg.group(0)+ "\t")
        outfile.write("\n")
        inputSTATlist = joinedtablefile.readline().strip(" ").split()

        for i in range(0,inputSTATlist.__len__()):
            if inputSTATlist[i].__contains__(difftarget):
                targetpos = i
                break
        for line in joinedtablefile.readlines():
            splitline = line.split()
            outfile.write(splitline[0] + "\t")
            del splitline[0]
            for val in range(0,pathlist.__len__()):
                del splitline[val+1]
                del splitline[val+1]
            diffvalue = float(splitline[targetpos])
            print(diffvalue)
            outfile.write(str(diffvalue) + "\t")
            for i in range(0,pathlist.__len__()):
                if i == targetpos:
                    continue
                outfile.write(str( diffvalue  - (float)(splitline[i]) ) + "\t")
                print(str(diffvalue) + " - " +splitline[i] )
                print((str( diffvalue  - (float)(splitline[i]) ) + "\t"))
            outfile.write('\n')

        outfile.close()
########################################################################################################################
class TestDataGenerator:
    """
    Generates random data for testing
    """
    def __init__(self):
        pass

    def produce_random_surflex_result(self):
        list = []
        list.append("ZINC")
        list.extend([str(random.randint(0,9)) for i in range(8) ])
        list.extend("_000")
        list.extend(" " + str(random.randint(0,9))+"."+str(random.randint(10,99)) )
        list.extend(" " + str(random.randint(0,9))+"."+str(random.randint(10,99)) )
        list.extend(" " + str(random.randint(0,9))+"."+str(random.randint(10,99)) )
        return "".join(list)


class Cutter:
    def __init__(self, files):
        self.files = [files]

    def cut(self, outfile_name,threshold=3):
        """
        Cut from list values below threshold (default 3)
        """
        outfile = open(outfile_name, 'w+')


        outfile.write("Input Files:\n")
        if self.files is not None:
            filelist= []
            for filename in self.files:
                filelist.append(open(filename, 'r+'))
                outfile.write(str(filelist[-1]) + '\n')

        else:
            print("No input files...\n Exiting")
            exit()
        outfile.write("Threshold value: " + str(threshold) + '\n')
        statset = filelist[0].readline()
        outfile.write("Package\t" + statset)
        for file in filelist:

            match = re.match(r".*(p\d+\.\d+).*", file.name)
            if match is not None:
                prefix = match.group(1)
            else:
                prefix = file.name
            for line in file.readlines():
                valid = True
                temporaryline = (line.rstrip()).split("\t")
                for field in temporaryline[1:]:
                    try:
                        tmp = float(field)
                    except ValueError:
                        tmp = -1000
                    if tmp >= threshold:
                            continue
                    else:
                            valid = False
                            break

                if valid:
                    outfile.write(prefix + "\t" + "\t".join(temporaryline) + '\n')

        return outfile_name


class MolParser:
    def __init__(self):
        pass

    def ParseFiles(self, scorefile_path, filelist, outfile_path):

        fin = open(scorefile_path, 'r')
        fout = open(outfile_path, 'w')
        line = fin.readline()
        while line:
            match = re.match("Package	ID.*\n", line)
            if match is not None:
                break
            line = fin.readline()
        fin.readline()

        cutlist = []
        for line in fin.readlines():
            cutlist.append((line.rstrip()).split("\t"))
        while (cutlist):
            recordlist = []
            recordlist.append(cutlist[0])
            del cutlist[0]

            while True:
                if cutlist.__len__() is not 0 and cutlist[-1][0] == recordlist[-1][0]:
                    recordlist.append(cutlist[0])
                    del cutlist[0]

                else:
                    break


            molfiles = glob.glob(filelist[0] + "/*" + recordlist[-1][0] + "*" )
            molfile = open(molfiles[0], 'r')
            molecules = molfile.read().split("\n@<TRIPOS>MOLECULE")
            del molecules[0]
            for record in recordlist:
                for molecule in molecules:
                    if record[1] == molecule[1:13]:
                        splitline = (molecule.split("\n", 2))
                    fout.write("@<TRIPOS>MOLECULE\n" + splitline[1] + "_13_" + recordlist[-1][0] + "_" + args.ref_STAT[0] +"\n" + splitline[2])
            del recordlist
            molfile.close()

