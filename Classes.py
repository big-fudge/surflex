__author__ = 'kajtek'


import re
import glob
import os


########################################################################################################################
########################################################################################################################
from os import mkdir


serviceflag = True

class BestScoreIsolator:
    """
    Klasa pozwalajaca na wyizolowanie ze zbioru rekordow w formacie:
    ZINC[LICZBA]_000 {OCENY}
    ZINC[LICZBA]_001 {OCENY}
    ZINC[LICZBA]_002 {OCENY}
    ...
    Tylko te o najwyzszej ocenie
    """
########################################################################################################################

    def __init__(self, f):
        """
        @param f:str - ciezkado pliku z wynikami dokowania
        """
        self.f = open(f, 'r')
        # print(self.f)
########################################################################################################################

    def genbest(self):
        """
        Generator kolejnych najwyzej ocenianych rekordow
        """
        for __line in self.f.readlines():
            __match = re.search(r'ZINC\d+_000', __line)
            if __match is not None:
                yield __line.strip('\n')

########################################################################################################################
########################################################################################################################


class CatalogParser:
    """
    Klasa przechodzaca przez katalog zbierajac dane dotyczace pojedynczego bialka
    szukajac plikow wg wzorca *STAT[liczba]*.log
    """
########################################################################################################################

    def __init__(self, path):
        """
        @param path:str - sciezka do katalogu z danymi
        """
        self.path = path
########################################################################################################################

    def listallSTATs(self):
        """
        Wypisuje i zwraca liste wszstkich plikow w katalogu zawierajacych slowo kluczowe STAT
        w postaci listy sciezek bezwzglednych
        """
        self.list = glob.glob(self.path + "/*STAT*.log")

        self.list.sort()
        __filelist = []
        for item in self.list:
            __filelist.append(item)
        # if serviceflag == True:
            # print("Wywolanie funkcji CatalogParser::listallSTATS\n")
            # for i in __filelist:
            #     print(i + " ")
            # print("**-Koniec funkcji CatalogParser::listallSTATS\n")
        return __filelist
########################################################################################################################

    def listsingleSTAT(self, name, absotulenames=False):
        """
        Zwraca w postaci listy nazwy (lub sciezki bezwzgledne) wszystkich plikow zawierajacych
        w nazwie STAT[liczba]

        Wymagana skladnia nazwy to: [dowolne-znaki]log_[nazwa]_[liczba]_p[liczba].[liczba][dowolne-znaki]

        @param absolutenames:Boolean - wyswietla sciezki bezwzgledne
        """
        self.list = glob.glob(self.path + "/*STAT*.log")
        self.list.sort()
        __regex = r'.*(log_' + name + r'_' + r'\d+' + r'_p\d+\.\d+.*)'
        # print("Regex: " + __regex)
        __filelist = []
        for __file in self.list:
            __match = re.search(__regex, __file)
            if __match is not None:
                if absotulenames:
                    # print(__file)
                    __filelist.append(__file)
                else:
                    # print(__match.group(1))
                    __filelist.append(__match.group(1))
        # if serviceflag == True:
        #     print("Wywolanie funkcji CatalogParser::listsingleSTATS\n")
        #     for i in __filelist:
        #         print(i + " ")
        #     print("**-Koniec funkcji CatalogParser::listsingleSTATS\n")
        return __filelist

########################################################################################################################

    def listSingleSTATTemporaryFiles(self):
        """
        Zwraca w postaci listy nazwy (lub sciezki bezwzgledne) wszystkich plikow tymczasowych zawierajacych
        slowo kluczowe STAT i rozszerzenie .temp

        @param absolutenames:Boolean - wyswietla sciezki bezwzgledne
        """
        self.list = glob.glob(self.path + "/STAT*.temp")
        self.list.sort()
        __filelist = []
        for item in self.list:
            __filelist.append(item)
        return __filelist
########################################################################################################################

    def getSmallestTempFile(self):
        """
        Zwraca sciezke pliku z najmniejsza liczba linii
        """
        shortest = str
        length = -1
        for file in self.listSingleSTATTemporaryFiles():
            if self.filelen(file) < length or length == -1:
                shortest = file
                length = self.filelen(file)
        return shortest

########################################################################################################################

    def filelen(self, fname):
        """
        Zwraca dlugosc danego pliku w liniach
        """
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

########################################################################################################################
########################################################################################################################

class STATJoiner:
    """
    Klasa pozwalajaca na laczenie danych z wielu plikow
    """
########################################################################################################################
    ##TODO Custom output filename
    def __init__(self, path, diffprot=[],temporarydir="Temporary_Files",differentialfile=""):
        self.path = path
        self.temporaryDir = temporarydir
        self.diffprot = diffprot
        self.cp = CatalogParser(self.path)
        self.createTemporarydir()
        self.differentialfile = differentialfile


########################################################################################################################
##TODO Zmana katalogu w kazdej funkcji
    def createTemporarydir(self):

        mkdir(self.path + '/' + self.temporaryDir)
        os.chdir(self.path + '/' + self.temporaryDir )
    ########################################################################################################################

    def generatefilename(self, file, diffprot):


        with open(file) as f:
            f.readline()
            tmp = f.readline()
            regex = re.search(".*(p.*)-result.*", tmp)
            if regex is not  None:
                return regex.group(1) +"_"+ str(diffprot) + "_differential_score"
            else:
                return str(diffprot) + "_differential_score"

    def joinWithinSTATs(self):
        """
        Laczy wyniki dla tych samych bialek STAT
        """
        uniquestats = set()


        for file in self.cp.listallSTATs():
            # print("**" + file)
            __match = re.search(r'.*(STAT\d+[abAB]?).*', file)
            if __match is not None:
                # print("***" + __match.group(1))
                uniquestats.add(__match.group(1))
        #Tworzenie nazwy pliku tymczasowego zawierajacego wszystkie, najlepsze wyniki dla danego bialka stat
        filenameregex = re.search(r'(.*)(STAT\d+[abAB]?)(.*)_p.*(-results.*)', self.cp.listallSTATs()[0])

        # for ui in uniquestats:
        #     print(ui)
        for uniqueitem in uniquestats: #dla kazdego bialka stat
            # outfile = open(
            #     (filenameregex.group(1) + uniqueitem + filenameregex.group(3) + filenameregex.group(4) + '-joint.log'),
            #     'w') #Otworz plik
            # self.temporaryDir = os.getcwd()
            outfile = open(uniqueitem + "_temporary.temp", 'w')
            outfile.write("#########USED FILES:\n")
            for file in self.cp.listsingleSTAT(uniqueitem):
                outfile.write('# ' + file + '\n')
            outfile.write('####################\n\n')
            for file in self.cp.listsingleSTAT(uniqueitem, True):
                # print("Lines in: \n" + file + " " + str(cp.filelen(file)))
                bs = BestScoreIsolator(file)
                for line in bs.genbest():
                    outfile.write(line + '\n')
    def joinAllSTATs(self):
        """
        Laczy wszystkie wyniki dot poszczegolnych bialek stat w pojedynczy plik wynikowy
        """

        cp = CatalogParser(self.temporaryDir)
        pathlist = list()
        pathlist.extend(cp.listSingleSTATTemporaryFiles())

        #Umieszczenie najmniejszego pliku na poczatku listy
        # TODO Kolejnosc się zgadza ?
        # tmp = cp.getSmallestTempFile()
        # tmp = pathlist[0]
        # pathlist.remove(tmp)
        # pathlist.insert(pathlist.__len__(), tmp)
        # pathlist.sort(key= lambda k: (str)(k[4:5]))
        print("Join All STATS: \n")
        # for key in pathlist:
        #     print(key)
        ###################################################

        # filelist = list()
        # filelist.append(open(cp.getSmallestTempFile(), 'r'))
        # print("current path: " + self.temporaryDir)
        dedup = Deduplicator(pathlist, self.temporaryDir)
        tabfile = dedup.generate()

######################################
        referencefile = open(self.filePathsList.pop())
        self.filelist = [open(file, 'r') for file in self.filePathsList]
        outfile = open(self.temporaryDir + "/joined_table.temp", 'w')
        outfile.write(" " +(re.search(r'(STAT\d+[abAB]?)', self.referencefile.name)).group(0) + "   ")
        for tmpfilename in self.filelist:
            outfile.write((re.search(r'(STAT\d+[abAB]?)', tmpfilename.name)).group(0) + "   ")
        outfile.write('\n')
        referencelines = self.referencefile.readlines()
        # referencelines.sort()
        cmplines = []
        for file in self.filelist:
            cmplines.append(file.readlines())

        for i in range(0,cmplines.__len__()):
            cmplines[i].sort()

##Niepotrzebne range
        for i in range(0,referencelines.__len__()):
            fixedline = []
            linematch = re.match(r'(ZINC\d+_000)', referencelines[i])
            if linematch is None:
                continue
            fixedline.append(referencelines[i].strip('\n'))
            [file.seek(0) for file in self.filelist]

            for i2 in range(0,cmplines.__len__()):
                for i3 in range(0, len(cmplines[i2])):
                    regexTemp = linematch.group(0) + r' (.*)'
                    innerLineMatch = re.match(regexTemp, cmplines[i2][i3])

                    if innerLineMatch is not None:
                        fixedline.append(' ' +innerLineMatch.group(1))
                        del cmplines[i2][i3]
                        break


            if fixedline.__len__() == self.filelist.__len__()+1:
              ##TODO print ponizej niepotrzebny
                # print (''.join(fixedline))
                outfile.write(''.join(fixedline) + '\n')
                # print(fixedline)

        return  outfile


##########################################


        # tabfile = "/home/kajtek/PycharmProjects/Skrypt_1/test/Temporary_Files/joined_table.temp"
        for prot in self.diffprot:
            di = Differential(tabfile, self.temporaryDir, pathlist, self.generatefilename(pathlist[pathlist.__len__()-1], prot))
            di.diff(prot)
########################################################################################################################

class Deduplicator:

    def __init__(self, filePathsList, outFilePath):

        self.filePathsList = list()
        self.filePathsList.extend(filePathsList)
        self.outFilePath = outFilePath
        self.openfiles()

    def openfiles(self):
       self.referencefile = open(self.filePathsList.pop())
       self.filelist = [open(file, 'r') for file in self.filePathsList]


    def generate(self):
    pass

class Differential:
    def __init__(self, joinedtablefile, outFilePath, filelist, outname):
        self.inputfile = open(joinedtablefile, 'r')
        self.outfile = open(outFilePath + "/../" + outname + ".tsv", 'w')
        self.filelist = list(filelist)

    def diff(self, difftarget):

        tmp = self.filelist.pop()
        self.filelist.insert(0,tmp)
        self.outfile.write("ID\t" + difftarget + "\t")
        for file in self.filelist:
            reg = re.search(r'(STAT\d+[abAB]?)', file)
            if reg is not None and not reg.group(0).__contains__(difftarget):
                self.outfile.write(difftarget + "-" + reg.group(0)+ "\t")
        self.outfile.write("\n")
        inputSTATlist = self.inputfile.readline().strip(" ").split()
        for i in range(0,inputSTATlist.__len__()):
            if inputSTATlist[i].__contains__(difftarget):
                targetpos = i
                break
        # print(targetpos)
        # print(inputSTATlist)
        for line in self.inputfile.readlines():
            splitline = line.split(" ")
            # print(splitline)
            # self.outfile.write((re.match(r'(ZINC\d+)', line)).group(0) + " ")
            self.outfile.write(splitline[0] + "\t")
            del splitline[0]
            for val in range(0,self.filelist.__len__()):
                del splitline[val+1]
                del splitline[val+1]
            # print(splitline)
            diffvalue = float(splitline[targetpos])
            # print(diffvalue)
            self.outfile.write(str(diffvalue) + "\t")
            for i in range(0,self.filelist.__len__()):
                if i == targetpos:
                    continue
                # differential = ((float)(splitline[i]) - diffvalue )
                self.outfile.write(str( diffvalue  - (float)(splitline[i]) ) + "\t")
            self.outfile.write('\n')

        self.outfile.close()